package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenheitTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testConvertFromCelsiusRegular() {
		assertEquals(32,Fahrenheit.convertFromCelsius(0));
	}
	@Test
	public final void testConvertFromCelsiusException() {
		assertNotEquals(-0,Fahrenheit.convertFromCelsius(0));
	}
	@Test
	public final void testConvertFromCelsiusBoundaryIn() {
		assertEquals(37,Fahrenheit.convertFromCelsius(3));
	}
	@Test
	public final void testConvertFromCelsiusBoundaryOut() {
		assertNotEquals(38,Fahrenheit.convertFromCelsius(4));
	}

}
