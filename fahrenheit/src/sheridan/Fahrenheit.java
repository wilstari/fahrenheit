package sheridan;

public class Fahrenheit {

	public static void main(String[]args) {
		System.out.println(convertFromCelsius(3));
		System.out.println(convertFromCelsius(-0));
		System.out.println(convertFromCelsius(4));
		System.out.println(convertFromCelsius(0));
	}
	
	
	public static int convertFromCelsius(int a) {
		return (int) Math.ceil((a*9/5 + 32));

}
}
